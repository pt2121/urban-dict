package com.pt2121.dictionary.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.jakewharton.rxbinding3.widget.textChanges
import com.pt2121.dictionary.R
import com.pt2121.dictionary.R.string
import com.pt2121.dictionary.home.model.InternalError
import com.pt2121.dictionary.home.model.IoError
import dagger.android.AndroidInjection
import io.reactivex.disposables.SerialDisposable
import kotlinx.android.synthetic.main.activity_main.editText
import kotlinx.android.synthetic.main.activity_main.progressBar
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_main.sortSwitch
import java.util.concurrent.TimeUnit.MILLISECONDS
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: MainActivityViewModel

    private val disposable = SerialDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val postAdapter = PostAdapter()
        recyclerView.adapter = postAdapter

        viewModel.postItems.observe(this, Observer { postItems ->
            postAdapter.submitList(postItems)
        })

        viewModel.errors.observe(this, Observer { error ->
            val message = when (error) {
                is InternalError -> getString(string.generic_error)
                is IoError -> error.message
            }
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        })

        viewModel.loading.observe(this, Observer { visible ->
            progressBar.visibility = if (visible) View.VISIBLE else View.GONE
        })

        disposable.set(editText.textChanges()
            .debounce(1500, MILLISECONDS)
            .subscribe { query ->
                viewModel.search(query.toString())
            })

        editText.requestFocus()

        sortSwitch.setOnCheckedChangeListener { _, isChecked ->
            viewModel.reorder(isChecked)
        }
    }

    override fun onDestroy() {
        disposable.dispose()
        super.onDestroy()
    }
}
