package com.pt2121.dictionary.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pt2121.data.DictionaryRepository
import io.reactivex.Scheduler
import javax.inject.Inject

class MainActivityViewModelFactory @Inject constructor(
    private val dictionaryRepository: DictionaryRepository,
    private val ioScheduler: Scheduler
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass != MainActivityViewModel::class.java) {
            throw IllegalArgumentException()
        }
        return MainActivityViewModel(
            dictionaryRepository,
            ioScheduler
        ) as T
    }
}
