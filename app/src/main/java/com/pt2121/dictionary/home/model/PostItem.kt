package com.pt2121.dictionary.home.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// UI-layer models for the adapter

@Parcelize
data class PostItem(
    val id: Long,
    val definition: String,
    val word: String,
    val author: String,
    val date: String,
    val thumbsUp: Int,
    val thumbsDown: Int
) : Parcelable