package com.pt2121.dictionary.home.model

import com.pt2121.data.model.Post
import java.text.DateFormat

fun Post.toPostItem(dateFormat: DateFormat) =
    PostItem(
        defid,
        definition,
        word,
        author,
        dateFormat.format(writtenOn),
        thumbsUp,
        thumbsDown
    )