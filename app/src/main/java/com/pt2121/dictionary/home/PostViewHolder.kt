package com.pt2121.dictionary.home

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pt2121.dictionary.R.id
import com.pt2121.dictionary.home.model.PostItem

class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val wordTextView: TextView = itemView.findViewById(id.wordTextView)
    private val definitionTextView: TextView = itemView.findViewById(id.definitionTextView)
    private val authorTextView: TextView = itemView.findViewById(id.authorTextView)
    private val dateTextView: TextView = itemView.findViewById(id.dateTextView)
    private val thumbsUpTextView: TextView = itemView.findViewById(id.thumbsUpTextView)
    private val thumbsDownTextView: TextView = itemView.findViewById(id.thumbsDownTextView)

    fun bind(postItem: PostItem) {
        wordTextView.text = postItem.word
        definitionTextView.text = postItem.definition
        authorTextView.text = postItem.author
        dateTextView.text = postItem.date
        thumbsUpTextView.text = postItem.thumbsUp.toString()
        thumbsDownTextView.text = postItem.thumbsDown.toString()
    }
}