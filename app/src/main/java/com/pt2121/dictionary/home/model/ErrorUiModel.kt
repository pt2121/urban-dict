package com.pt2121.dictionary.home.model

sealed class ErrorUiModel
object InternalError : ErrorUiModel()
data class IoError(val message: String) : ErrorUiModel()