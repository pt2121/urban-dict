package com.pt2121.dictionary.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pt2121.data.DictionaryRepository
import com.pt2121.dictionary.home.model.ErrorUiModel
import com.pt2121.dictionary.home.model.InternalError
import com.pt2121.dictionary.home.model.IoError
import com.pt2121.dictionary.home.model.PostItem
import com.pt2121.dictionary.home.model.toPostItem
import io.reactivex.Scheduler
import io.reactivex.disposables.SerialDisposable
import io.reactivex.rxkotlin.subscribeBy
import java.text.DateFormat

class MainActivityViewModel(
    private val dictionaryRepository: DictionaryRepository,
    private val ioScheduler: Scheduler
) : ViewModel() {

    private val disposable = SerialDisposable()

    private val dateFormat = DateFormat.getDateInstance()

    private val _postItems = MutableLiveData<List<PostItem>>()
    val postItems: LiveData<List<PostItem>>
        get() = _postItems

    private val _errors = MutableLiveData<ErrorUiModel>()

    val errors: LiveData<ErrorUiModel>
        get() = _errors

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private var sortByThumbsUp = true

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    fun reorder(thumbsUp: Boolean) {
        sortByThumbsUp = thumbsUp
        val selector = getSortBySelector()
        _postItems.postValue(_postItems.value?.sortedByDescending(selector))
    }

    fun search(query: String) {
        if (query.isBlank()) {
            _postItems.postValue(emptyList())
        } else {
            _loading.postValue(true)
            disposable.set(
                dictionaryRepository.search(query)
                    .subscribeOn(ioScheduler)
                    .subscribeBy(onError = { throwable ->
                        _loading.postValue(false)
                        _errors.postValue(IoError(throwable.localizedMessage))
                    }, onSuccess = { result ->
                        _loading.postValue(false)

                        if (result.isSuccess) {
                            result.map { posts ->
                                val items = posts.map {
                                    it.toPostItem(dateFormat)
                                }
                                val selector = getSortBySelector()
                                _postItems.postValue(items.sortedByDescending(selector))
                            }
                        } else {
                            _errors.postValue(InternalError)
                        }

                    })
            )
        }
    }

    private fun getSortBySelector(): (PostItem) -> Int = { postItem: PostItem ->
        if (sortByThumbsUp) postItem.thumbsUp else postItem.thumbsDown
    }
}
