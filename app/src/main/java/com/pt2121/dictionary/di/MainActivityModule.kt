package com.pt2121.dictionary.di

import android.app.Activity
import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.pt2121.dictionary.home.MainActivity
import com.pt2121.dictionary.home.MainActivityViewModel
import com.pt2121.dictionary.home.MainActivityViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

@Module
abstract class MainActivityModule {

    @Binds
    abstract fun provideActivity(mainActivity: MainActivity): FragmentActivity

    @Binds
    abstract fun context(activity: Activity): Context

    @Module
    companion object {

        @JvmStatic
        @Provides
        fun mainActivityViewModel(
            factory: MainActivityViewModelFactory,
            fragmentActivity: FragmentActivity
        ): MainActivityViewModel {
            return ViewModelProviders.of(fragmentActivity, factory).get(MainActivityViewModel::class.java)
        }

        @JvmStatic
        @Provides
        fun ioScheduler(): Scheduler = Schedulers.io()
    }
}