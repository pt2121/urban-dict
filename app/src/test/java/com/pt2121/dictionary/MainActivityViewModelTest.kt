package com.pt2121.dictionary

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.pt2121.data.DictionaryRepository
import com.pt2121.dictionary.home.MainActivityViewModel
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import kotlin.Result

@RunWith(JUnit4::class)
class MainActivityViewModelTest {

    @Rule
    @JvmField
    val instantExecutor = InstantTaskExecutorRule()
    private val repository = mock(DictionaryRepository::class.java)
    private lateinit var viewModel: MainActivityViewModel
    private val ioScheduler = TestScheduler()

    @Before
    fun init() {
        viewModel = MainActivityViewModel(repository, ioScheduler)
    }

    @Test
    fun search_basic() {
        `when`(repository.search("test"))
            .thenReturn(Single.just(Result.success(emptyList())))

        viewModel.search("test")

        verify(repository).search("test")
    }

    @Test
    fun search_emptyQuery() {
        viewModel.search("")

        verify(repository, never()).search("")
    }
}