package com.pt2121.data

import com.pt2121.data.api.DictionaryService
import com.pt2121.data.db.dao.PostDao
import com.pt2121.data.model.DictResponse
import com.pt2121.data.model.Post
import io.reactivex.Single
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.*
import org.junit.Assert.*
import org.mockito.Mockito.*
import retrofit2.Response
import java.util.Calendar
import java.util.GregorianCalendar

class DictionaryRepositoryTest {

    abstract class FakeDictionaryService : DictionaryService {
        override fun search(
            apiKey: String,
            term: String
        ): Single<Response<DictResponse>> {
            return Single.just(getPostsResponse())
        }

        abstract fun getPostsResponse(): Response<DictResponse>
    }

    private val post1 = Post(
        definition = "LOL",
        permalink = "",
        thumbsUp = 1,
        thumbsDown = 2,
        author = "Prat",
        word = "LOL",
        defid = 0,
        example = "example 1",
        writtenOn = GregorianCalendar(2019, Calendar.MAY, 7).time
    )

    private val post2 = Post(
        definition = "lol",
        permalink = "",
        thumbsUp = 1,
        thumbsDown = 2,
        author = "Prat",
        word = "lol",
        defid = 1,
        example = "example 2",
        writtenOn = GregorianCalendar(2019, Calendar.MAY, 8).time
    )

    private val responseSuccess = Response.success(DictResponse(posts = listOf(post1, post2)))!!

    private val responseError = Response.error<DictResponse>(
        400,
        ResponseBody.create(MediaType.parse(""), "Error")
    )!!

    @Test
    fun search_success() {
        // given
        val repository = makeRepo(responseSuccess)

        // when
        val data = repository.search("lol").blockingGet()

        // then
        assertEquals(Result.success(listOf(post1, post2)), data)
    }

    @Test
    fun search_error() {
        // given
        val repository = makeRepo(responseError)

        // when
        val data = repository.search("lol").blockingGet()

        // then
        assertTrue(data.isFailure)
    }

    private fun makeRepo(fakeResult: Response<DictResponse>): DictionaryRepository {
        val fakeService = object : FakeDictionaryService() {
            override fun getPostsResponse(): Response<DictResponse> =
                fakeResult
        }

        val timeLimitChecker = object : TimeLimitChecker {
            override fun shouldFetch(key: String): Boolean = true
        }

        val postDao = mock(PostDao::class.java)

        return DictionaryRepository(
            DictionaryRemoteDataSource(fakeService, ""),
            postDao,
            timeLimitChecker
        )
    }
}
