package com.pt2121.data.model

import com.google.gson.annotations.SerializedName

data class DictResponse(
    @SerializedName("list") val posts: List<Post>
)