package com.pt2121.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.Date

@Entity(
    tableName = "posts"
)
data class Post(
    @ColumnInfo(name = "definition")
    @SerializedName("definition") val definition: String,

    @ColumnInfo(name = "permalink")
    @SerializedName("permalink") val permalink: String,

    @ColumnInfo(name = "thumbs_up")
    @SerializedName("thumbs_up") val thumbsUp: Int,

    @ColumnInfo(name = "thumbs_down")
    @SerializedName("thumbs_down") val thumbsDown: Int,

    @ColumnInfo(name = "author")
    @SerializedName("author") val author: String,

    @ColumnInfo(name = "word")
    @SerializedName("word") val word: String,

    @PrimaryKey
    @ColumnInfo(name = "defid")
    @SerializedName("defid") val defid: Long,

    @ColumnInfo(name = "example")
    @SerializedName("example") val example: String,

    @ColumnInfo(name = "written_on")
    @SerializedName("written_on") val writtenOn: Date
)