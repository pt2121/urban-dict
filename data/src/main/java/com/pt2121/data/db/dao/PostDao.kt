package com.pt2121.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.pt2121.data.model.Post

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(posts: List<Post>)

    @Transaction
    @Query("SELECT * FROM posts WHERE lower(word) = :word")
    fun getPosts(word: String): List<Post>
}