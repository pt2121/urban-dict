package com.pt2121.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pt2121.data.db.dao.PostDao
import com.pt2121.data.model.Post

@Database(
    entities = [Post::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class DictDb : RoomDatabase() {

    abstract fun postDao(): PostDao

    companion object {
        private const val DATABASE_NAME = "dictionary-db"

        @Volatile
        private var instance: DictDb? = null

        fun getInstance(context: Context): DictDb {
            return instance ?: synchronized(this) {
                instance ?: Room.databaseBuilder(
                    context, DictDb::class.java,
                    DATABASE_NAME
                ).build().also { instance = it }
            }
        }
    }
}