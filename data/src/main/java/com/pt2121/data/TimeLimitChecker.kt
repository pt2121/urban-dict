package com.pt2121.data

import android.os.SystemClock
import androidx.collection.ArrayMap
import java.util.concurrent.TimeUnit

interface TimeLimitChecker {
    fun shouldFetch(key: String): Boolean
}

class TimeLimitCheckerImpl : TimeLimitChecker {

    private val timestamps = ArrayMap<String, Long>()
    private val timeout = TimeUnit.MINUTES.toMillis(60)

    @Synchronized
    override fun shouldFetch(key: String): Boolean {
        val lastFetched = timestamps[key]
        val now = SystemClock.uptimeMillis()
        if (lastFetched == null) {
            timestamps[key] = now
            return true
        }
        if (now - lastFetched > timeout) {
            timestamps[key] = now
            return true
        }
        return false
    }
}