package com.pt2121.data.api

import com.pt2121.data.model.DictResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query

interface DictionaryService {

    @Headers(
        "X-RapidAPI-Host: mashape-community-urban-dictionary.p.rapidapi.com"
    )
    @GET("define")
    fun search(
        @Header("X-RapidAPI-Key") apiKey: String,
        @Query("term") term: String
    ): Single<Response<DictResponse>>

    companion object {
        const val ENDPOINT = "https://mashape-community-urban-dictionary.p.rapidapi.com/"
    }
}