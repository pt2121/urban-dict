package com.pt2121.data

import com.pt2121.data.api.DictionaryService
import com.pt2121.data.model.Post
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject

class DictionaryRemoteDataSource @Inject constructor(
    private val dictionaryService: DictionaryService,
    private val apiKey: String
) {

    fun search(term: String): Single<Result<List<Post>>> =
        dictionaryService.search(
            apiKey = apiKey,
            term = term
        ).map { response ->
            if (response.isSuccessful) {
                val posts = response.body()?.posts
                if (posts != null) {
                    return@map Result.success(posts)
                }
            }
            return@map Result.failure<List<Post>>(
                IOException("${response.code()} ${response.errorBody() ?: ""}")
            )
        }
}