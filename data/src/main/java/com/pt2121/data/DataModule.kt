package com.pt2121.data

import android.app.Application
import com.google.gson.GsonBuilder
import com.pt2121.data.api.DictionaryService
import com.pt2121.data.db.DictDb
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class DataModule {

    @Singleton
    @Provides
    fun provideDictionaryService(): DictionaryService {
        val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()
        return Retrofit.Builder()
            .baseUrl(DictionaryService.ENDPOINT)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(DictionaryService::class.java)
    }

    @Singleton
    @Provides
    fun provideDictionaryDataSource(
        dictionaryService: DictionaryService,
        apiKey: String
    ): DictionaryRemoteDataSource =
        DictionaryRemoteDataSource(dictionaryService, apiKey)

    @Singleton
    @Provides
    fun provideDictDb(context: Application): DictDb =
        DictDb.getInstance(context)

    @Singleton
    @Provides
    fun provideDictionaryRepository(
        dictionaryRemoteDataSource: DictionaryRemoteDataSource,
        db: DictDb,
        timeLimitChecker: TimeLimitChecker
    ): DictionaryRepository =
        DictionaryRepository(dictionaryRemoteDataSource, db.postDao(), timeLimitChecker)

    @Singleton
    @Provides
    fun provideTimeLimitChecker(): TimeLimitChecker =
        TimeLimitCheckerImpl()

    @Singleton
    @Provides
    fun apiKey(): String = BuildConfig.API_KEY
}