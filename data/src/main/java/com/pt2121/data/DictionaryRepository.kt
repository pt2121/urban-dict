package com.pt2121.data

import com.pt2121.data.db.dao.PostDao
import com.pt2121.data.model.Post
import io.reactivex.Single

class DictionaryRepository(
    private val dictionaryRemoteDataSource: DictionaryRemoteDataSource,
    private val postDao: PostDao,
    private val timeLimitChecker: TimeLimitChecker
) {

    fun search(term: String): Single<Result<List<Post>>> {
        val word = term.toLowerCase()
        return if (timeLimitChecker.shouldFetch(word)) {
            fetchFromRemote(term)
        } else {
            val list = postDao.getPosts(word)
            if (list.isNotEmpty()) {
                Single.just(Result.success(list))
            } else {
                fetchFromRemote(term)
            }
        }
    }

    private fun fetchFromRemote(term: String): Single<Result<List<Post>>> =
        dictionaryRemoteDataSource.search(term)
            .doAfterSuccess { result ->
                result.map { post ->
                    postDao.insertAll(post)
                }
            }
}